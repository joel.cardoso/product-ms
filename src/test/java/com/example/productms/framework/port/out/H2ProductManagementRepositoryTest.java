package com.example.productms.framework.port.out;

import com.example.productms.application.port.out.ProductManagementPortOut;
import com.example.productms.domain.model.Product;
import com.example.productms.framework.adapter.out.persistence.H2ProductManagementRepository;
import com.example.productms.framework.adapter.out.persistence.ProductManagementRepository;
import com.example.productms.framework.adapter.out.persistence.entity.ProductEntity;
import com.example.productms.framework.exceptions.ResourceNotFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.List;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
@DataJpaTest
class H2ProductManagementRepositoryTest {

    @InjectMocks
    ProductManagementPortOut productManagementPortOut = new H2ProductManagementRepository();

    @Mock
    ProductManagementRepository productManagementRepository;

    @MockBean
    ModelMapper modelMapper;

    @Test
    @DisplayName("Deve salvar um livro")
    void saveProductTest() {
        Product product = Product.builder().name("Iphone 12").description("Apple Iphone 12").price(12.000).build();
        ProductEntity productToSave = ProductEntity.builder().name("Iphone 12").description("Apple Iphone 12").price(12.000).build();
        ProductEntity savedEntity = ProductEntity.builder().id(18).name("Iphone 12").description("Apple Iphone 12").price(12.000).build();
        Product productToReturn = Product.builder().id(18).name("Iphone 12").description("Apple Iphone 12").price(12.000).build();
        BDDMockito.when( modelMapper.map(product, ProductEntity.class) ).thenReturn(productToSave);
        BDDMockito.when( productManagementRepository.save(productToSave) ).thenReturn( savedEntity );
        BDDMockito.when( modelMapper.map(savedEntity, Product.class) ).thenReturn(productToReturn);

        Product savedProduct = productManagementPortOut.save(product);

        Assertions.assertNotNull(savedProduct.getId());
        Assertions.assertEquals(savedProduct.getId(), savedEntity.getId());
    }

    @Test
    @DisplayName("Deve buscar um produto por id")
    void findProductByIdTest() {
        Integer productId = 18;
        Product productToReturn = Product.builder().id(18).name("Iphone 12").description("Apple Iphone 12").price(12.000).build();
        ProductEntity productEntity = ProductEntity.builder().id(18).name("Iphone 12").description("Apple Iphone 12").price(12.000).build();
        BDDMockito.when( productManagementRepository.findById(productId)).thenReturn(Optional.ofNullable(productEntity));
        BDDMockito.when( modelMapper.map(productEntity, Product.class) ).thenReturn(productToReturn);

        Product product = productManagementPortOut.findProductById(productId);
        Assertions.assertNotNull(product.getId());
        Assertions.assertSame(product.getId(), productEntity.getId());
    }

    @Test
    @DisplayName("Deve lançar erro not found ao tentar buscar um produto inexistente")
    void throwNotFound() {
        Exception exception = Assertions.assertThrows(ResourceNotFoundException.class, () -> productManagementPortOut.findProductById(Mockito.anyInt()));
        Assertions.assertSame("Produto não encontrado", exception.getMessage());
    }

    @Test
    @DisplayName("Deve retornar todos os produtos")
    void findAllProducts() {
        ProductEntity productEntity = ProductEntity.builder().id(18).name("Iphone 12").description("Apple Iphone 12").price(12.000).build();
        List<ProductEntity> productEntities = List.of(productEntity);
        BDDMockito.when( productManagementRepository.findAll() ).thenReturn(productEntities);

        List<Product> products = productManagementPortOut.findAll();

        Assertions.assertNotNull(products);
    }

    @Test
    @DisplayName("Deve retornar os produtos filtrados")
    void findByCustomValues() {
        ProductEntity productEntity = ProductEntity.builder().id(18).name("Iphone 12").description("Apple Iphone 12").price(12.000).build();
        List<ProductEntity> productEntities = List.of(productEntity);
        Product product = Product.builder().id(18).name("Iphone 12").description("Apple Iphone 12").price(12.000).build();
        List<Product> productList = List.of(product);
        BDDMockito.when( productManagementRepository.findByCustomValues("Iphone", "Iphone", null, null) ).thenReturn(productEntities);

        List<Product> filteredProducts = productManagementPortOut.findProductByCustomValues("Iphone", "Iphone", null, null);

        Assertions.assertNotNull(filteredProducts);
    }

    @Test
    @DisplayName("Deve deletar um produto por id")
    void deleteProductById() {
        Integer productId = Mockito.anyInt();

        productManagementPortOut.deleteById(productId);

        Mockito.verify(productManagementRepository, Mockito.times(1)).deleteById(productId);
    }
}
