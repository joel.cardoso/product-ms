package com.example.productms.framework.exceptions;

import lombok.Builder;
import lombok.Data;

import java.io.Serializable;

@Data
@Builder
public class ExceptionResponse implements Serializable {
    private static final long serialVersionUID = 1L;

    private Integer status_code;
    private String message;
}
