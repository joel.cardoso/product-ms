package com.example.productms.framework.port.in;

import com.example.productms.application.port.in.ProductManagementUseCase;
import com.example.productms.domain.model.Product;
import com.example.productms.framework.adapter.in.rest.ProductController;
import com.example.productms.framework.dto.ProductDTO;
import com.example.productms.framework.exceptions.ResourceNotFoundException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.List;
import java.util.Locale;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(MockitoExtension.class)
@WebMvcTest(controllers = ProductController.class)
@AutoConfigureMockMvc
class ProductControllerTest {

    static String PRODUCT_MANAGEMENT_API = "/products";

    @Autowired
    MockMvc mockMvc;

    @MockBean
    ProductManagementUseCase productManagementUseCase;

    @Test
    @DisplayName("Deve salvar um produto com sucesso")
    void saveProductTest() throws Exception {
        ProductDTO productDTO = ProductDTO.builder().name("Iphone 12").description("Apple Iphone 12").price(10.000).build();
        Product savedProduct = Product.builder().id(1).name("Iphone 12").description("Apple Iphone 12").price(10.000).build();
        BDDMockito.given( productManagementUseCase.saveProduct(Mockito.any(Product.class)) ).willReturn( savedProduct );
        String requestBody = new ObjectMapper().writeValueAsString(productDTO);

        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders
                .post(PRODUCT_MANAGEMENT_API)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(requestBody);

        mockMvc
        .perform(requestBuilder)
        .andExpect(status().isCreated())
        .andExpect(jsonPath("id").isNotEmpty())
        .andExpect(jsonPath("name").value(productDTO.getName()))
        .andExpect(jsonPath("description").value(productDTO.getDescription()))
        .andExpect(jsonPath("price").value(productDTO.getPrice()));

    }

    @Test
    @DisplayName("Deve lançar bad request ao tentar salvar um produto com dados insuficientes")
    void saveInvalidProductTest() throws Exception {
        String errorMessage = "Client specified an invalid argument, request body o query param";
        String requestBody = new ObjectMapper().writeValueAsString(ProductDTO.builder().build());

        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders
                .post(PRODUCT_MANAGEMENT_API)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(requestBody);

        mockMvc.perform(requestBuilder)
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("message").value(errorMessage))
                .andExpect(jsonPath("status_code").value(400));
    }

    @Test
    @DisplayName("Deve atualizar o produto com sucesso")
    void updateProductById() throws Exception {
        ProductDTO productDTO = ProductDTO.builder().name("Iphone 12").description("Apple Iphone 12").price(14.000).build();
        Product updatingProduct = Product.builder().name("Iphone 12").description("Apple Iphone 12").price(14.000).build();
        BDDMockito.given( productManagementUseCase.updateProduct(updatingProduct, 12) ).willReturn( Product.builder().id(12).name("iphone 12").description("apple iphone 12").price(14.000).build() );
        String requestBody = new ObjectMapper().writeValueAsString(productDTO);

        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders
                .put(PRODUCT_MANAGEMENT_API.concat("/" + 12))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(requestBody);

        mockMvc.perform(requestBuilder)
                .andExpect(status().isOk())
                .andExpect(jsonPath("id").isNotEmpty())
                .andExpect(jsonPath("name").value(productDTO.getName().toLowerCase(Locale.ROOT)))
                .andExpect(jsonPath("description").value(productDTO.getDescription().toLowerCase(Locale.ROOT)))
                .andExpect(jsonPath("price").value(productDTO.getPrice()));
    }

    @Test
    @DisplayName("Deve lançar not found ao tentar atualizar produto inexistente")
    void updateProductNotFoundException() throws Exception {
        String errorMessage = "Resource Not Found";
        ProductDTO productDTO = ProductDTO.builder().name("Iphone 12").description("Apple Iphone 12").price(14.000).build();
        Product updatingProduct = Product.builder().name("Iphone 12").description("Apple Iphone 12").price(14.000).build();
        BDDMockito.given( productManagementUseCase.updateProduct(updatingProduct, 5566) ).willThrow(ResourceNotFoundException.class);
        String requestBody = new ObjectMapper().writeValueAsString(productDTO);

        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders
                .put(PRODUCT_MANAGEMENT_API.concat("/" + 5566))
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(requestBody);

        mockMvc.perform(requestBuilder)
                .andExpect(status().isNotFound())
                .andExpect(jsonPath("message").value(errorMessage))
                .andExpect(jsonPath("status_code").value(404));
    }

    @Test
    @DisplayName("Deve retornar um produto por id")
    void findProductByIdTest() throws Exception {
        Integer productId = Mockito.anyInt();
        Product product = Product.builder().id(productId).name("Ultrabook").description("Ultrabook Lenovo").price(1290.000).build();
        BDDMockito.when( productManagementUseCase.getProductById(productId) ).thenReturn(product);

        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get(PRODUCT_MANAGEMENT_API.concat("/" + productId)).accept(MediaType.APPLICATION_JSON);

        mockMvc.perform(requestBuilder)
                .andExpect(status().isOk())
                .andExpect(jsonPath("id").value(product.getId()))
                .andExpect(jsonPath("name").value(product.getName()))
                .andExpect(jsonPath("description").value(product.getDescription()))
                .andExpect(jsonPath("price").value(product.getPrice()));
    }

    @Test
    @DisplayName("Deve retornar todos os produtos")
    void getAllProducts() throws Exception {
        Product product = Product.builder().id(12).name("Ultrabook").description("Ultrabook Lenovo").price(1290.000).build();
        List<Product> productList = List.of(product);
        BDDMockito.when( productManagementUseCase.getAllProducts() ).thenReturn( productList );

        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get(PRODUCT_MANAGEMENT_API).accept(MediaType.APPLICATION_JSON);

        mockMvc.perform(requestBuilder)
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].id").value(product.getId()));
    }

    @Test
    @DisplayName("Deve retornar os produtos filtrados")
    void getProductsByCustomValues() throws Exception {
        Product product = Product.builder().id(12).name("Ultrabook").description("Ultrabook Lenovo").price(1290.000).build();
        List<Product> productList = List.of(product);
        String QRequestParam = "Ultrabook";
        BDDMockito.when( productManagementUseCase.getProductsByCustomValues(QRequestParam, null, null) ).thenReturn( productList );

        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.get(PRODUCT_MANAGEMENT_API.concat("/search")).param("q", QRequestParam);

        mockMvc.perform(requestBuilder)
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].id").value(product.getId()))
                .andExpect(jsonPath("$[0].name").value(product.getName()));
    }

    @Test
    @DisplayName("Deve deletar um produto")
    void deleteBookById() throws Exception {
        Integer productId = 12;
        BDDMockito.doNothing().when( productManagementUseCase ).deleteProductById(productId);

        MockHttpServletRequestBuilder requestBuilder = MockMvcRequestBuilders.delete(PRODUCT_MANAGEMENT_API.concat("/" + productId));

        mockMvc.perform(requestBuilder)
                .andExpect(status().isOk());
    }
}
