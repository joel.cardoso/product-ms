package com.example.productms.framework.adapter.out.persistence;

import com.example.productms.framework.adapter.out.persistence.entity.ProductEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductManagementRepository extends JpaRepository<ProductEntity, Integer> {

    @Query("SELECT p FROM ProductEntity p WHERE" + "(:name IS NULL OR LOWER (p.name) LIKE LOWER(CONCAT('%',:name,'%')) OR :description IS NULL OR LOWER (p.description) LIKE  LOWER(CONCAT('%',:description,'%')))"
                                                 + "AND (:minPrice IS NULL OR p.price >= :minPrice)"
                                                 + "AND (:maxPrice IS NULL OR p.price <= :maxPrice)")
    List<ProductEntity> findByCustomValues(@Param("name") String name, @Param("description") String description, @Param("minPrice") Double minPrice, @Param("maxPrice") Double maxPrice);
}
