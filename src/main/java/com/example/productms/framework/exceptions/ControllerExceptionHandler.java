package com.example.productms.framework.exceptions;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ControllerExceptionHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(ControllerExceptionHandler.class);

    @ExceptionHandler(ResourceNotFoundException.class)
    public ResponseEntity<ExceptionResponse> handleNotFoundExceptions(ResourceNotFoundException resourceNotFoundException) {
        ExceptionResponse exceptionResponse = ExceptionResponse.builder().status_code(404).message("Resource Not Found").build();
        LOGGER.error("Resource Not Found {}", resourceNotFoundException.getMessage());
        return new ResponseEntity<>(exceptionResponse, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ExceptionResponse> handleValidationExceptions(MethodArgumentNotValidException argumentNotValidException) {
        ExceptionResponse exceptionResponse = ExceptionResponse.builder().status_code(400).message("Client specified an invalid argument, request body o query param").build();
        LOGGER.error("MethodArgumentNotValidException {}", argumentNotValidException.getMessage());
        return new ResponseEntity<>(exceptionResponse, HttpStatus.BAD_REQUEST);
    }
}
