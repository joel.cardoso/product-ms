package com.example.productms.application.port.in;

import com.example.productms.domain.model.Product;

import java.util.List;

public interface ProductManagementUseCase {

    Product saveProduct(Product product);
    Product updateProduct(Product product, Integer productId);
    Product getProductById(Integer productId);
    List<Product> getAllProducts();
    List<Product> getProductsByCustomValues(String productNameOrDescription, Double minPrice, Double maxPrice);
    void deleteProductById(Integer productId);
}
