package com.example.productms.application.service;

import com.example.productms.application.port.in.ProductManagementUseCase;
import com.example.productms.application.port.out.ProductManagementPortOut;
import com.example.productms.domain.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductManagementUserImpl implements ProductManagementUseCase {

    @Autowired
    ProductManagementPortOut productManagementPortOut;

    @Override
    public Product saveProduct(Product product) {
        Product productToSave = Product.builder()
                .name(product.getName())
                .description(product.getDescription())
                .price(product.getPrice()).build();
        return productManagementPortOut.save(productToSave);
    }

    @Override
    public Product updateProduct(Product product, Integer productId) {
        Product productToUpdate = productManagementPortOut.findProductById(productId);
        product.setName(product.getName());
        product.setDescription(product.getDescription());
        product.setId(productToUpdate.getId());
        return productManagementPortOut.save(product);
    }

    @Override
    public Product getProductById(Integer productId) {
        return productManagementPortOut.findProductById(productId);
    }

    @Override
    public List<Product> getAllProducts() {
        return productManagementPortOut.findAll();
    }

    @Override
    public List<Product> getProductsByCustomValues(String productNameOrDescription, Double minPrice, Double maxPrice) {
        return productManagementPortOut.findProductByCustomValues(productNameOrDescription, productNameOrDescription, minPrice, maxPrice);
    }

    @Override
    public void deleteProductById(Integer productId) {
        getProductById(productId);
        productManagementPortOut.deleteById(productId);
    }
}
