package com.example.productms.framework.adapter.out.persistence;

import com.example.productms.application.port.out.ProductManagementPortOut;
import com.example.productms.domain.model.Product;
import com.example.productms.framework.adapter.out.persistence.entity.ProductEntity;
import com.example.productms.framework.exceptions.ResourceNotFoundException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class H2ProductManagementRepository implements ProductManagementPortOut {

    @Autowired
    ProductManagementRepository productManagementRepository;

    @Autowired
    ModelMapper modelMapper;

    @Override
    public Product save(Product product) {
        ProductEntity productEntity = productManagementRepository.save(modelMapper.map(product, ProductEntity.class));
        return modelMapper.map(productEntity, Product.class);
    }

    @Override
    public Product findProductById(Integer productId) {
        Optional<ProductEntity> optionalProductEntity = productManagementRepository.findById(productId);
        ProductEntity productEntity = optionalProductEntity.stream().findFirst().orElseThrow(() -> new ResourceNotFoundException("Produto não encontrado"));
        return modelMapper.map(productEntity, Product.class);
    }

    @Override
    public List<Product> findAll() {
        List<ProductEntity> productEntities = productManagementRepository.findAll();
        return productEntities.stream().map(entity -> modelMapper.map(entity, Product.class)).collect(Collectors.toList());
    }

    @Override
    public List<Product> findProductByCustomValues(String productName, String productDescription, Double minPrice, Double maxPrice) {
        List<ProductEntity> productEntities = productManagementRepository.findByCustomValues(productName, productDescription, minPrice, maxPrice);
        return productEntities.stream().map(entity -> modelMapper.map(entity, Product.class)).collect(Collectors.toList());
    }

    @Override
    public void deleteById(Integer productId) {
        productManagementRepository.deleteById(productId);
    }
}
