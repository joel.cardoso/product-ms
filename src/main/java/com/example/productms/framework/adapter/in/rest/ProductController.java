package com.example.productms.framework.adapter.in.rest;

import com.example.productms.application.port.in.ProductManagementUseCase;
import com.example.productms.domain.model.Product;
import com.example.productms.framework.dto.ProductDTO;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/products")
public class ProductController {

    @Autowired
    ProductManagementUseCase productManagementUseCase;

    @Autowired
    ModelMapper modelMapper;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<ProductDTO> saveProduct(@RequestBody @Valid  ProductDTO productDTO) {
        Product product = modelMapper.map(productDTO, Product.class);
        Product savedProduct = productManagementUseCase.saveProduct(product);
        URI uri = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}").buildAndExpand(savedProduct.getId()).toUri();
        return ResponseEntity.created(uri).body(modelMapper.map(savedProduct, ProductDTO.class));
    }

    @PutMapping("/{productId}")
    public ResponseEntity<ProductDTO> updateProduct(@RequestBody @Valid ProductDTO productDTO, @PathVariable Integer productId) {
        Product product = modelMapper.map(productDTO, Product.class);
        Product updatedProduct = productManagementUseCase.updateProduct(product, productId);
        return ResponseEntity.ok().body(modelMapper.map(updatedProduct, ProductDTO.class));
    }

    @DeleteMapping("/{productId}")
    public ResponseEntity<Void> deleteProduct(@PathVariable Integer productId) {
        productManagementUseCase.deleteProductById(productId);
        return ResponseEntity.ok().build();
    }

    @GetMapping
    public ResponseEntity<List<ProductDTO>> getAllProducts() {
        List<Product> productList = productManagementUseCase.getAllProducts();
        List<ProductDTO> productDTOList = productList.stream().map(product -> modelMapper.map(product, ProductDTO.class)).collect(Collectors.toList());
        return ResponseEntity.ok().body(productDTOList);
    }

    @GetMapping("/search")
    public ResponseEntity<List<ProductDTO>> getProductsByCustomValues(@RequestParam(name = "q", required = false) String nameOrDescription, @RequestParam(name = "min_price", required = false) Double minPrice, @RequestParam(name = "max_price", required = false) Double maxPrice) {
        List<Product> productList = productManagementUseCase.getProductsByCustomValues(nameOrDescription, minPrice, maxPrice);
        List<ProductDTO> productDTOList =  productList.stream().map(product -> modelMapper.map(product, ProductDTO.class)).collect(Collectors.toList());
        return ResponseEntity.ok().body(productDTOList);
    }

    @GetMapping("/{productId}")
    public ResponseEntity<ProductDTO> getProductById(@PathVariable Integer productId) {
        Product product = productManagementUseCase.getProductById(productId);
        return ResponseEntity.ok(modelMapper.map(product, ProductDTO.class));
    }
}
