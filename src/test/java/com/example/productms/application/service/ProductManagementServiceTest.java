package com.example.productms.application.service;

import com.example.productms.application.port.in.ProductManagementUseCase;
import com.example.productms.application.port.out.ProductManagementPortOut;
import com.example.productms.domain.model.Product;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

@ExtendWith(MockitoExtension.class)
class ProductManagementServiceTest {

    @InjectMocks
    ProductManagementUseCase productManagementUseCase = new ProductManagementUserImpl();

    @Mock
    ProductManagementPortOut productManagementPortOut;

    @Test
    @DisplayName("Deve salvar um novo produto e retornar o mesmo com o id gerado")
    void saveNewProductTest() {
        Product product = Product.builder().name("smart tv").description("smart tv panasonic 43 polegadas").price(100.5).build();
        BDDMockito.when( productManagementPortOut.save(product) )
                .thenReturn(Product.builder()
                        .id(1)
                        .name("smart tv")
                        .description("smart tv panasonic 43 polegadas")
                        .price(100.5).build());
        Product savedProduct = productManagementUseCase.saveProduct(product);
        Assertions.assertNotNull(savedProduct.getId());
        Assertions.assertEquals(product.getName(), savedProduct.getName());
    }

    @Test
    @DisplayName("Deve atualizar um produto")
    void updateProductTest() {
        Product productToUpdate = Product.builder().name("Iphone 12").description("Apple Iphone 12").price(100.00).build();
        BDDMockito.when( productManagementPortOut.findProductById(12) ).thenReturn( Product.builder().id(12).name("Iphone 12").description("Apple Iphone 12").price(100.00).build() );
        BDDMockito.when( productManagementPortOut.save(productToUpdate) ).thenReturn( Product.builder().id(12).name("Iphone 12").description("Apple Iphone 12").price(100.00).build() );

        Product updatedProduct = productManagementUseCase.updateProduct(productToUpdate, 12);

        Assertions.assertNotNull(updatedProduct);
        Assertions.assertEquals(productToUpdate.getName(), updatedProduct.getName());
        Assertions.assertEquals(productToUpdate.getDescription(), updatedProduct.getDescription());
        Assertions.assertEquals(productToUpdate.getPrice(), updatedProduct.getPrice());
        Assertions.assertEquals(productToUpdate.getId(), updatedProduct.getId());
    }

    @Test
    @DisplayName("Deve retornar um produto por id")
    void findProductById() {
        Integer productId = Mockito.anyInt();
        Product product = Product.builder().id(productId).name("Ultrabook").description("Ultrabook Lenovo").price(120.00).build();
        BDDMockito.when( productManagementPortOut.findProductById(productId) ).thenReturn(product);

        Product returnedProduct = productManagementUseCase.getProductById(productId);

        Assertions.assertNotNull(returnedProduct);
        Assertions.assertEquals(returnedProduct.getId(), productId);
    }

    @Test
    @DisplayName("Deve retornar todos os produtos")
    void getAllProducts() {
        Product product = Product.builder().id(12).name("Ultrabook").description("Ultrabook Lenovo").price(120.00).build();
        List<Product> productList = List.of(product);
        BDDMockito.when( productManagementPortOut.findAll() ).thenReturn(productList);

        List<Product> products = productManagementUseCase.getAllProducts();

        Assertions.assertNotNull(products);
        Assertions.assertIterableEquals(products, productList);
    }

    @Test
    @DisplayName("Deve retornar todos os produtos filtrados")
    void getProductsByCustomValues() {
        Product product = Product.builder().id(12).name("Ultrabook").description("Ultrabook Lenovo").price(120.00).build();
        List<Product> productList = List.of(product);
        BDDMockito.when( productManagementPortOut.findProductByCustomValues("Ultrabook", "Ultrabook", null, null) ).thenReturn(productList);

        List<Product> products = productManagementUseCase.getProductsByCustomValues("Ultrabook", null, null);

        Assertions.assertNotNull(products);
        Assertions.assertIterableEquals(products, productList);
    }

    @Test
    @DisplayName("Deve deletar um produto por id")
    void deleteProductById() {
        Integer productId = Mockito.anyInt();

        Assertions.assertDoesNotThrow(() -> productManagementUseCase.deleteProductById(productId));

        Mockito.verify(productManagementPortOut, Mockito.times(1)).deleteById(productId);
    }
}
