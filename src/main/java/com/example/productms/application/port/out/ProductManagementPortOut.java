package com.example.productms.application.port.out;

import com.example.productms.domain.model.Product;

import java.util.List;

public interface ProductManagementPortOut {
    Product save(Product product);
    Product findProductById(Integer productId);
    List<Product> findAll();
    List<Product> findProductByCustomValues(String productName, String productDescription, Double minPrice, Double maxPrice);
    void deleteById(Integer productId);
}
